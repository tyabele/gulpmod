//scoped reusabled gulp tasks to use from project to project
module.exports = function(gulp, instanceConfig, path){
    var $ = require('gulp-load-plugins')({
      lazy: true,
      camelize: true
    }),
    config = _updateConfig(require('./gulp.config')(), instanceConfig),
    colors = $.util.colors,
    del = require('del'),
    pathModule = require('path'),
    mainBowerFiles = require('main-bower-files'),
    configurator = require("./utils/configurator"),
    codeGenerator = require('./utils/generator'),
    ftp = require('vinyl-ftp'),
    argv = require('yargs').argv;


if(!config.appName)
  throw new Error('Please set an appName in gulp.config to use jx-gulp-module');
//web server
var express = require('express'),
    livereload = require('connect-livereload'),
    livereloadport = 35729,
    serverport = 3000;

    var env = argv.env;
//Check to see if the machine config is available...
try{
    config.machine = require('./machine.config.json');
}catch(e){
    config.machine = undefined;
}

// Default Task
gulp.task('default', ['server', 'watch']);

gulp.task('generate', function() {
    log('Deleting old files...', colors.green);
    del.sync(['public/app/shared/models/*.js']);
    del.sync(['public/app/shared/data/*.js']);
    del.sync(['public/app/shared/validation/*.js']);
    var _config = configurator.getEnvConfigObj(config.JuxlyServers);
    return codeGenerator.generate(_config);
});
  gulp.task('inject-ng', function() {
    return addAppFilesToIndex();
  });

//
// Build a common config file based off the machine.config.file || environmental variables.
//
gulp.task("build-config", function(done){
    log("Generating App Config files", colors.green);
    // Juxly Servers to consume from...
    // Usage: name: <nameForUseInAngular>, url:"<genericBaseUrlForServer>"
    // NOTE! The Url will have https & the env prefix added.

    // Gulp 4 is picky ... call done when the sync task is completed.
    // http://stackoverflow.com/a/36899424/121466 Option 3
    var _config = configurator.getEnvConfigObj(config.JuxlyServers);
    configurator.create(_config);
    done();
});

// Bower Stuff...
gulp.task('bower-files', ['custom-bower-repos'], function() {
    del.sync(['./public/lib']);
    gulp.src(mainBowerFiles(['**/*.js', '**/*.css']))
    .pipe(gulp.dest("./public/lib"));
});

gulp.task('custom-bower-repos', function() {
    gulp.src(['bower_components/juxly-client-core/**'])
    .pipe(gulp.dest("./public/lib/juxly-client-core"));
});

gulp.task('inject-bower-files', function() {
    gulp.src(config.injectBase, {
            base: './'
        })
        .pipe($.inject(gulp.src(config.allLib, {
            read: false,
            cwd: path + '/public/'
        }), {name: 'vendor'}))
        .pipe(gulp.dest(config.base));
});

// Inject dynamic js references into index.template.html
// and output to ./public/index.html
gulp.task('inject-index', function() {
    return gulp.src(config.injectBase, {
        base: config.base
    })
        .pipe($.inject(
            gulp.src(config.libJs, {read: false, 'cwd': path + '/public/'}).pipe($.order(config.libJsOrder, {base: 'public'})),
                 { name: 'libJs', addRootSlash: false, ignorePath: 'public', removeTags: false }
        ))
        .pipe($.inject(
            gulp.src(config.appJs, {'cwd': path + '/public/'})
            .pipe($.angularFilesort()), { name: 'appJs', addRootSlash: false, ignorePath: 'public', removeTags: false }
        ))
        .pipe($.rename(config.index))
        .pipe(gulp.dest(config.injectDest));
});

gulp.task('rename-index', ['build-tasks'], function() {
  if(config.appName === config.timelineAppName) return;
    return gulp.src(config.injectBase, {
        base: config.base
    })
    .pipe($.rename(config.index))
    .pipe(gulp.dest(config.base + 'dist'));
});

////////////////////////
// Linting and Lessing Tasks
////////////////////////
gulp.task('lint', function() {
    return gulp.src(config.appJs)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'));
});

gulp.task('less', function () {
    return compileLessFile('./assets/less/styles.less',
        'public/stylesheets/');
});

///////////////////
// Server Tasks
///////////////////
  var server = express();
  server.use(livereload({
      port: livereloadport
  }));

  server.use(express.static(path + '/public/'));
  server.all('/*', function(req, res) {
      res.sendFile(config.index, {
        root: path + '/public/'
      });
  });

  gulp.task('server', function() {
      //start web server
      server.listen(serverport, '0.0.0.0');
      //start live reload
      $.livereload.listen(livereloadport);
  });

  // Watch Files For Changes
  gulp.task('watch', ['lint', 'less'], function() {
      gulp.watch(config.appJs, ['lint']);
      gulp.watch('assets/less/*.less', ['less']);
      gulp.watch(['public/app/**/*', '!app/**/*.less'])
              .on('change', $.livereload.changed);
  });

////////////////////////////////////////////////////////////////
// Deploy
///////////////////////////////////////////////////////////////
// Push a fresh version of the client code into the C# web root
// note: takes an optional --destination argument
gulp.task('deploy-client', function(done) {
    var useCustomDestination = (argv.destination === undefined) ? false : true;
    if (useCustomDestination) {
        config.webRoot = argv.destination;
    }
    log('Deploying new files to ' + config.webRoot, colors.green);
    gulp.src('./dist/**/*').pipe(gulp.dest(config.webRoot));
});

gulp.task('clean-client', function() {
    log('Deleting files from previous deployment...', colors.green);
    del.sync(['./public/index.html'], {force: true});
    del.sync([config.webRoot], {force: true});
});

/// --------- TEMPORARY ONLY ---------------------------------
/// --------- TEMPORARY ONLY ---------------------------------
/// --------- TEMPORARY ONLY ---------------------------------

  gulp.task('deploy-timelinetemp', ['build'], function() {
    var creds = _getFtpConnection();
    var conn = ftp.create(creds);
    var globs = [
      'auth/**',
      'bin/**',
      'dist/**', // compiled public
      'integrations/**',
      '!integrations/*/config.json',
      'routes/**',
      'views/**',
      'app.js',
      'iisnode.yml',
      'package.json', // we'll have to install packages manually, ftp node_modules is too problematic
      'Web.config'
    ];
    if (!conn || !conn.rmdir) return log('Unable to connect to ftp');
    upload();
    function upload(err) {
      if (err)
        console.log(err);
      if (conn && conn.disconnect === 'function')
        conn.disconnect();
      log('Uploading project to web server');
      return gulp.src(globs, { base: path, buffer: false } )
          //.pipe($.debug())
          .pipe(conn.newerOrDifferentSize('/site/wwwroot'))
          .pipe(conn.dest('/site/wwwroot'));
    }

    function _getFtpConnection() {
      log(process.env.JxFTPUser);
      log(process.env.JxFTPPassword);
      log(process.env.JxFTPFilePath);
      log(process.env.JxFTPHost);
      if (!process.env.JxFTPUser || !process.env.JxFTPPassword || !process.env.JxFTPFilePath || !process.env.JxFTPHost)
        throw new Error('FTP Configuration not set for environment');
      return {
        user: process.env.JxFTPUser,
        pass: process.env.JxFTPPassword,
        remotePath: process.env.JxFTPFilePath,
        host: process.env.JxFTPHost,
        parallel: 10,
        secure: true,
        log: $.util.log
      };
    }
  });

/// --------- TEMPORARY ONLY ---------------------------------
/// --------- TEMPORARY ONLY ---------------------------------
/// --------- TEMPORARY ONLY ---------------------------------

  gulp.task('deploy', ['build'], function() {
    var creds = _getFtpConnection();
    var conn = ftp.create(creds);
    if (!conn || !conn.rmdir) return log('Unable to connect to ftp');
    upload();
    function upload(err) {
      if (err)
        log(err);
      if (conn && conn.disconnect === 'function')
        conn.disconnect();
      log('Uploading project to web server');
      return gulp.src(path + '/dist/**', { buffer: false } )
          .pipe($.debug())
          .pipe(conn.newerOrDifferentSize('/site/wwwroot'))
          .pipe(conn.dest('/site/wwwroot'));
    }

    function _getFtpConnection() {
      log(process.env.JxFTPUser);
      log(process.env.JxFTPPassword);
      log(process.env.JxFTPFilePath);
      log(process.env.JxFTPHost);
      if (!process.env.JxFTPUser || !process.env.JxFTPPassword || !process.env.JxFTPFilePath || !process.env.JxFTPHost)
        throw new Error('FTP Configuration not set for environment');
      return {
        user: process.env.JxFTPUser,
        pass: process.env.JxFTPPassword,
        remotePath: process.env.JxFTPFilePath,
        host: process.env.JxFTPHost,
        parallel: 10,
        log: $.util.log
      };
    }
  });
////////////////////////////////////////////////////////////////
// Builds
///////////////////////////////////////////////////////////////

  ////////////////////
  // Build Tasks

    gulp.task('build', ['rename-index'], function() {
      return gulp.src('dist/**', {read: false})
          .pipe($.util.noop());
    });

    gulp.task('build-tasks', ['cleandist', 'createDistIndex', 'moveFonts', 'moveImages', 'moveIntegrationCss', 'moveStylesheets', 'moveStatic', 'move-integrations'], function() {
        return gulp.src('dist/**', {read: false})
          .pipe($.util.noop());
    });

      gulp.task('cleandist', function() {
          return del.sync(['dist']);
      });

      gulp.task('createDistIndex', ['templatecache', 'scripts', 'compile-vendor', 'compile-css', 'compile-vendor-css','distHtml'], function() {
        //Injects file elements into the index.html...
        return gulp.src(config.injectBase, {
            base: config.base
          })
          .pipe($.inject(gulp.src([config.appName + '.js'], {
              cwd: path + '/dist/',
                    ignorePath: 'dist',
                    addRootSlash: false
          }), {name: 'appJs'}))
          .pipe($.inject(gulp.src(['vendor.js'], {
              cwd: path + '/dist/',
                    ignorePath: 'dist',
                    addRootSlash: false
          }), {name: 'libJs'}))
          .pipe($.inject(gulp.src([config.appName + '.css'], {
            cwd: path + '/dist/',
                  ignorePath: 'dist',
                  addRootSlash: false
          }), {name: 'styles'}))
          .pipe($.inject(gulp.src(['vendor.css'], {
            cwd: path + '/dist/',
                  ignorePath: 'dist',
                  addRootSlash: false
          }), {name: 'libstyles'}))
          .pipe(gulp.dest(config.base));
      });

      gulp.task('templatecache', function() {
        return gulp.src(config.htmlTemplates, {cwd: path + '/public/'})
        .pipe($.angularTemplatecache(config.templateCache.file,
          config.templateCache.options))
        .pipe(gulp.dest('dist'));
      });


      gulp.task('scripts', function () {
          return gulp.src(config.appJs, {cwd: path + '/public/'})
            .pipe($.plumber())
            .pipe($.ngAnnotate())
            .pipe($.angularFilesort())
            .pipe($.uglify())
            .pipe($.concat(config.appName + '.js'))
            .pipe(gulp.dest('dist'));
      });

      gulp.task('compile-vendor', function() {
        return gulp.src(config.libJs, {cwd: path + '/public/'})
          .pipe($.order(config.libJsOrder, {base: 'public'}))
          .pipe($.plumber())
          .pipe($.uglify())
          .pipe($.concat('vendor.js'))
          .pipe(gulp.dest('dist'));
      });

      ////////
    // CSS Tasks & Compilation...
      gulp.task('compile-css', function() {
        return gulp.src(config.appCss, {cwd: path + '/public/'})
          .pipe($.order(config.appCss, {base: 'public'}))
          .pipe($.plumber())
          .pipe($.minifyCss({keepSpecialComments: false, processImport: true}))
          .pipe($.concat(config.appName + '.css'))
          .pipe(gulp.dest('dist'));
      });

      gulp.task('compile-vendor-css', function() {
        return gulp.src(config.libCss, {cwd: path + '/public/'})
          .pipe($.order(config.libCss, {base: 'public'}))
          .pipe($.plumber())
          .pipe($.minifyCss({keepSpecialComments: false, processImport: true}))
          .pipe($.concat('vendor.css'))
          .pipe(gulp.dest('dist'));
      });

      gulp.task('moveIntegrationCss', function() {
        return gulp.src(['assets/styles/allscripts.css', 'assets/styles/cerner.css', 'assets/styles/athena.css'], {cwd: path + '/public/'})
          .pipe(gulp.dest('dist/assets/styles'));
      });

      gulp.task('distHtml', ['libHtml'], function(){
        //Minifys the html before moving it to the dist directory.
        var opts = {
          loose:true,
          empty:true,
            conditionals: true,
            spare:true
        };
          return gulp.src(['app/**/*.html'], {cwd: path + '/public/'})
            .pipe($.minifyHtml(opts))
            .pipe(gulp.dest('./dist/app'));
      });

      gulp.task('libHtml', function() {
          return gulp.src(['lib/**/*.html'], {cwd: path + '/public/'})
            .pipe(gulp.dest('./dist/lib'));
      });

      gulp.task('moveFonts', function() {
        return gulp.src('assets/fonts/**/', {cwd: path + '/public/'})
          .pipe(gulp.dest('dist/assets/fonts'));
      });

      gulp.task('moveImages', function() {
        return gulp.src('images/**/', {cwd: path + '/public/'})
          .pipe(gulp.dest('dist/images'));
      });

      gulp.task('moveStatic', function (){
        return gulp.src('jxstatic/**/', {cwd: path + '/public/'})
        .pipe(gulp.dest('dist/jxstatic'))
      })

      gulp.task('moveStylesheets', function() {
        return gulp.src('stylesheets/**/', {cwd: path + '/public/'})
          .pipe(gulp.dest('dist/stylesheets'));
      });

      gulp.task('move-integrations', function() {
          return gulp.src(['app/allscripts-integration.service.js',
        'app/cerner-integration.service.js', 'app/athena-integration.service.js', 'app/app.js'], {cwd: path + '/public/'})
          //.pipe($.uglify())
          .pipe(gulp.dest('./dist/app'));
      });

  ////////
  // Config

    //Configure build, set-up common.config, inject files, and lint
    gulp.task('config', ['removeconfig'], function() {
      return gulp.src('common.config.json')
      .pipe($.ngConstant({
        name: 'common',
        templatePath: 'configtemplate.tpl.ejs',
        constants: {
          ENV: _createEnvObject()
        }
      }))
      .pipe(gulp.dest('public/app/common'));
    });

    gulp.task('removeconfig', function() {
      return gulp.src(config.commonConfigFile, {
        read: false,
        cwd: path + '/public/'
      })
      .pipe($.rimraf());
    });

/////////////////////////////////////////////////////////////////
// Testing
////////////////////////////////////////////////////////////////
  gulp.task('karma', function() {
    return gulp.src(['fakefilepathbecausekarmaisawesome'], {read: false})
      .pipe($.plumber())
      .pipe($.karma({
        configFile: config.karmaConfig,
        action: 'run'
      }));
  });
  gulp.task('webdriver_update', $.protractor.webdriver_update);
  gulp.task('webdriver_standalone', $.protractor.webdriver_standalone);

  gulp.task('test', function() {
      gulp.src(config.specs, {read: false})
          .pipe($.protractor.protractor({
              configFile: path + "/tests/e2e/protractor.conf.js",
              args: ['--baseUrl', 'http://localhost:5000/']
          }))
           .on('end', function(e) {
            log('E2E Testing complete');
            process.exit();
          })
          .on('error', function(e) {
            if(server && server.close) server.close();
            log('E2E Tests Failed');
            log(JSON.stringify(e));
            process.exit(1);
          });
    });


////////////////////////////////////////////////////////////////
// FUNCTIONS
////////////////////////////////////////////////////////////////
  function log(msg, color) {
    if (!color) color = colors.magenta;
    if (typeof(msg) == 'object') {
      for (var item in msg) {
        if (msg.hasOwnProperty(item))
          $.util.log(color(msg[item]));
      }
    } else
      $.util.log(color(msg));
  }

  function addAppFilesToIndex() {
    log('Adding js files to index');
    return gulp.src(config.injectBase, {
        base: config.base
      })
      .pipe($.inject(
        gulp.src(config.appJs, {'cwd': path + '/public/'})
        .pipe($.angularFilesort())
      ))
      .pipe(gulp.dest(config.base));
  }

  function compileLessFile(dir, dest){
      gulp.src(dir)
      .pipe($.less({
          paths: [ pathModule.join(__dirname, 'less', 'includes') ]
          })
      )
      .pipe(gulp.dest(dest));
  }

  function _createEnvObject() {
      if (env === 'prod')
        return config.prodConfig;
      else if (env === 'test')
        return config.testConfig;
      return config.devConfig;
  }

  function _updateConfig(config, instanceConfig) {
    Object.keys(instanceConfig).forEach(function(key) {
        config[key] = instanceConfig[key];
    });
    return config;
  }
};