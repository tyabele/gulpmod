(function() {
    /**
     *  Creates skeleton of AngularJS module, controller, constants, and states file.
     */

    /*jshint node: true*/
    'use strict';

    var fs = require('fs'),
        Mustache = require('mustache'),
        _ = require('lodash'),
        sourceUtility = require('./sourceutility').SourceUtility;

    var getCode = function(opts, type) {
        var data = {
            moduleName: opts.moduleName,
            upperCaseModuleName: opts.moduleName.toUpperCase()
        };
        var sources = [];
        sources.push({'Type': 'Module', 'Source': createModule(data)});
        sources.push({'Type': 'Constant', 'Source': createConstants(data)});
        sources.push({'Type': 'State', 'Source': createStates(data)});
        sources.push({'Type': 'Controller', 'Source': createController(data)});

        _.forEach(sources, function (s) {
            s.Source = sourceUtility.makePristine(s.Source);
        });
        return sources;
    };

    var createModule = function(data) {
        var modTpl = fs.readFileSync(__dirname + '/templates/angular-module.mustache', 'utf-8');
        return Mustache.render(modTpl, data);
    };

    var createConstants = function(data) {
        var conTpl = fs.readFileSync(__dirname + '/templates/angular-constants.mustache', 'utf-8');
        return Mustache.render(conTpl, data);
    };

    var createStates = function(data) {
        var statesTpl = fs.readFileSync(__dirname + '/templates/angular-states.mustache', 'utf-8');
        return Mustache.render(statesTpl, data);
    };

    var createController = function(data) {
        var ctrlTpl = fs.readFileSync(__dirname + '/templates/angular-controller.mustache', 'utf-8');
        return Mustache.render(ctrlTpl, data);
    };

    var Scaffold = {
        getCode: function(opts) {
            return getCode(opts, 'angular');
        }
    };

    module.exports.Scaffold = Scaffold;
})(module.exports);