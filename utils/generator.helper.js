(function() {
    /*jshint node: true*/
    'use strict';
    var _ = require('lodash');
    var fs = require('fs');
    var validationCodeGen = require('./honestabe').ValidationCodeGen;

    function getEntities(aSchema) {
        return _.omit(_.uniq(_.map(aSchema.paths, function(path) {
            var req = path.post || path.get;
            if(req)  return _cleanTags(req.tags.toString());
        })), _.isUndefined);
    }

    function _cleanTags(val) {
        return val.replace('[', '').replace(']', '');
    }

    function createSwaggerObject(schema, entityName) {
        var obj = {};
        var models = [];
        obj.apis = [];
        obj.models = {};
        _.forOwn(schema.paths, function(path, key) {
            _.forOwn(path, function(op, method) {
                var name = _cleanTags(op.tags.toString());
                if(name !== entityName) return;
                var entityObj = _createEntityObject(op, key, entityName, method);
                obj.apis.push(entityObj);
                if (_.findIndex(models, function(mod) { return mod == entityObj.operations[0].type; }) < 0 &&
                    entityObj.operations[0].type != 'Object' && entityObj.operations[0].type != 'IHttpActionResult')
                        models.push(entityObj.operations[0].type);
            });
        });
        if (models.indexOf(entityName) < 1)
            models.push(entityName);
        _.forEach(schema.definitions, function(value, key) {
            _.forEach(models, function(model) {
                if (key == model) {
                    obj.models[key] = value;
                    obj.models[key].id = key;
                }
            });
        });
        obj.url = schema.schemes[0] + "://" + schema.host;
        return obj;
    }

    function _createEntityObject(operation, key, entityName, method) {
        return {
            path: key,
            operations: [{
                method: method.toUpperCase(),
                nickname: operation.operationId,
                parameters: _generateParameters(operation.parameters),
                type: _generateOperationType(operation.responses, entityName),
                items: _generateItems(operation.responses)
            }]
        };
    }

    function writeCode(code, entity) {
        _.forEach(code, function(c) {
            if (c.Type === 'Service') {
                if (entity == 'EnumResponse') entity = 'Enum';
                fs.writeFileSync('./public/app/shared/data/' + camelCase(entity) + '.dataservice.js', c.Source);
            } else if (c.Type === 'Model') {
                fs.writeFileSync('./public/app/shared/models/' + camelCase(c.EntityName) + '.modelfactory.js', c.Source);
            }
        });
    }

    function camelCase(word) {
        var tokens = [];
        word.split('-').forEach(function(token, index) {
            if (index === 0) {
                tokens.push(token[0].toLowerCase() + token.substring(1));
            } else {
                tokens.push(token[0].toUpperCase() + token.substring(1));
            }
        });
        return tokens.join('');
    }

    function generateValidation(entitySchema, entity) {
        if(entity.indexOf('[') > -1) return;
        var validationCode = validationCodeGen.getAngularValidationCode({
            className: entity,
            moduleName: 'shared',
            schema: JSON.stringify(entitySchema)
        });
        if (!_.isEmpty(validationCode) && !_.isUndefined(validationCode)) {
            fs.writeFileSync('./public/app/shared/validation/' + camelCase(entity) + 'Validation.factory.js', validationCode);
        }
    }

    function _generateParameters(params) {
        if(!params || params.length < 1) return [];
        return _.map(params, function(p) {
            return {
                paramType: p.in,
                name: p.name,
                description: '',
                required: p.required,
                type: _getParamType(p.schema)
            };
        });
    }

    function _getParamType(typeSchema) {
        if (!typeSchema)
            return '';
        if (typeSchema.$ref)
            return _chopPars(typeSchema.$ref.replace('#/definitions/', ''));
        if (typeSchema.items.$ref)
            return _chopPars(typeSchema.items.$ref.replace('#/definitions/', ''));
    }

    function _chopPars(val) {
        var index = val.indexOf('[');
        if (index > 0)
            val = val.substring(0, val.length - index);
        return val;
    }

    function _generateOperationType(responses, entityName) {
        var type = '';
        _.forOwn(responses, function(response) {
            var ref = '';
            if (response.schema.items)
                ref = response.schema.items.$ref;
            else
                ref = response.schema.$ref;

            if (response.description == 'OK' && ref) {
                if (ref.indexOf('Object') < 0)
                    type = ref.replace('#/definitions/', '');
                else
                    type = 'IHttpActionResult';
            } else
                type = entityName;
        });
        return type;
    }

    function _generateItems(responses) {
        var items = {};
        _.forEach(responses, function(response) {
            if (response.description == 'OK') {
                if (response.schema && response.schema.items)
                    items = response.schema.items;
                else
                    items = '';
            }
        });

        _.forOwn(items, function(value, key) {
            items[key] = value.replace('#/definitions/', '');
        });
        return items;
    }

    module.exports.CodeGenerationHelper = {
        getEntities: getEntities,
        createSwaggerObject: createSwaggerObject,
        writeCode: writeCode,
        generateValidation: generateValidation
    };
})(module.exports);
