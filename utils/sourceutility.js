(function() {
    /**
     *  Utility for formatting and cleansing generated source code
     */

    /*jshint node: true*/
    'use strict';

    var beautify = require('js-beautify').js_beautify,
        lint = require('jshint').JSHINT,
        _ = require('lodash');

    var beautifySource = function(src) {
        return beautify(src, {
            max_preserve_newlines: 2,
            indent_with_tabs: false,
            preserve_newlines: true,
            space_in_paren: false,
            jslint_happy: false,
            brace_style: 'collapse',
            keep_array_indentation: false,
            keep_function_indentation: false,
            eval_code: false,
            unescape_strings: false,
            break_chained_methods: false,
            e4x: false,
            wrap_line_length: 0
        });
    };

    var lintSource = function(src) {
        lint(src, {
            node: false,
            browser: false,
            undef: true,
            strict: true,
            trailing: true,
            smarttabs: true,
            validthis: true,
            sub: true,
            newcap: true,
            predef: [ '_' ]
        });
        lint.errors.forEach(function(error) {
            throw new Error('JSLint Error ' + lint.errors[0].reason + ' in ' + lint.errors[0].evidence);
        });
    };

    var makeSourcePristine = function(source) {
        var beautifulSource = [];
        lintSource(source);
        source = beautifySource(source);
        beautifulSource.push(source);
        return beautifulSource;
    };

    var SourceUtility = {
        makePristine: function(source) {
            return makeSourcePristine(source);
        }
    };

    module.exports.SourceUtility = SourceUtility;
})(module.exports);