(function() {
    /**
     *  Creates an AngularJS validation object from json-schema
     */

    /*jshint node: true*/
    'use strict';

    var fs = require('fs'),
        Mustache = require('mustache'),
        _ = require('lodash'),
        sourceUtility = require('./sourceutility').SourceUtility;

    var getCode = function(opts, type) {
        var data = {
            schemaData: opts.schema,
            className: opts.className,
            moduleName: opts.moduleName
        };
        var tpl = fs.readFileSync(__dirname + '/templates/angular-validation-class.mustache', 'utf-8');
        var src = Mustache.render(tpl, data);
        return sourceUtility.makePristine(src);
    };

    var ValidationCodeGen = {
        getAngularValidationCode: function(opts) {
            return getCode(opts, 'angular');
        }
    };

    module.exports.ValidationCodeGen = ValidationCodeGen;
})(module.exports);