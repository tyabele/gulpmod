(function() {
    //Configurator:
    // Builds the common.config.js file for the project.
    // Based off of the  machine.config.js file.
    //

    /*jshint node: true*/
    'use strict';


    var _ = require('lodash');
    var del = require("del");
    var fs = require('fs');
    var mustache = require('mustache');

    // Locals:
    var _commonConfigDirectory = "";
    var environment = process.env.JxENV;
    console.log(environment);

    var machine;
    try {
        machine = require('../../../machine.config.json');
    }
    catch(e){
        console.log(e);
        // This is a check around if the machine config file is available..
         machine = undefined;
    }



    function _initialize(_config){
        var commonDirectory = _config.directories.common;
        //backup and remove the old common config.
        var source = commonDirectory + "common.config.js";
        var target = commonDirectory + 'common.config.bu';
        var cpFile = _copyFile(source, target, function(err){
            if(!_.isUndefined(err)){
                console.log("Something went wrong while copying a backup of the common.config.js file");
            }
        });
        try{
            del.sync([commonDirectory + "common.config.js"]);
        } catch(e){
            throw("common.config.js delete failed - ooopser");
        }
    }

    function create(_config){
        _initialize(_config);
        var templateSource = _buildConfigTemplate(_config);
        _saveToDir(templateSource, _config.directories.common + "common.config.js");
    }


    //
    // Internal Functions...
    //

    function getEnvConfigObj(servers){
        // Return a generic stub to build the common.config.js file from.
        // Uses env variables over a local machine config file. Errors if none are available.
        // Note:
        // When adding a new server / api to consume, add it to your local
        // machine.config.json file, and here to ensure it's available when deployed.
        var prefix, envName, sendErrors, localStorageKey, onSuccessState, startNavigation, proxyURI, apiURI, firebaseApiKey, messageSenderId, storageBucket, firebaseTimeline;
        try{
            prefix = process.env.JxEnvPrefix || machine.settings.prefix;
            envName = process.env.JxENV || machine.settings.name;
            sendErrors = _.isUndefined(process.env.JxEnvSendErrors) ? machine.settings.sendErrors : process.env.JxEnvSendErrors;
            localStorageKey = process.env.JxLocalStorageKey || machine.settings.localStorageKey;
            onSuccessState = process.env.JxOnSuccessState || machine.settings.onSuccessState;
            startNavigation = process.env.JxStartNavigation || machine.settings.startNavigation;
            proxyURI = process.env.JxProxyURI || '';
            apiURI = process.env.JxApiURI || '';
            fhirURI = process.env.JxFhirURI|| '';
            firebaseApiKey = process.env.JxFirebaseAPIKey || '';
            messageSenderId = process.env.JxMessagingSenderId || '';
            storageBucket = process.env.JxStorageBucket || '';
            firebaseTimeline = process.env.JxFirebaseTimeline || '';
        } catch(e){
            throw("Failed... Missing ENV Variables or (If working locally) a Machine Config File. ");
        }
        var rv = {
            servers:[],
            settings:{
                name: envName,
                proxyURI: proxyURI,
                sendErrors: sendErrors,
                localStorageKey: localStorageKey,
                onSuccessState: onSuccessState,
                startNavigation: startNavigation,
                apiURI: apiURI,
                fhirURI: apiURI,
                firebaseTimeline: firebaseTimeline,
                storageBucket: storageBucket,
                messageSenderId: messageSenderId,
                firebaseAPIKey: firebaseApiKey
            },
            directories:{
                common : "public/app/common/"
            }
        };
        //Dynamicaly add the servers to the servers obj...
        _.forEach(servers, function(s){
            var isLocal = s.url.indexOf('0778') >= 0;
            var protocol = isLocal ? 'http://' : 'https://';
            var appliedPrefix = isLocal ? '' : prefix;
            var serverObj = {name: s.name, url: protocol + appliedPrefix + s.url +"/"};
            rv.servers.push(serverObj);
            if(s.isAuth){
                rv.tokenUrl = serverObj.url;
            }
        });
        return rv;
    }

    // Builds a stream for the common config tempalte..
    function _buildConfigTemplate(data){
        var template = fs.readFileSync(__dirname + '/templates/jx-common-config.mustache', 'utf-8');
        return mustache.render(template, data);
    }

    //Save some source to a file...
    function _saveToDir(source, dir){
        console.log("Attempting to save file to: " + dir);
        fs.writeFileSync(dir, source);
        console.log("File saved: " + dir);
    }

    //Copy a file
    function _copyFile(source, target, cb) {
        var cbCalled = false;
        var rd = fs.createReadStream(source);
        rd.on("error", function(err) {
            done(err);
        });

        var wr = fs.createWriteStream(target);
        wr.on("error", function(err) {
            done(err);
        });
        wr.on("close", function(ex) {
            done();
        });

        rd.pipe(wr);

        function done(err) {
            if (!cbCalled) {
                  cb(err);
                  cbCalled = true;
            }
        }
    }


    module.exports = {
        getEnvConfigObj: getEnvConfigObj,
        create: create
    };
})(module.exports);