(function() {
    /*jshint node: true*/
    'use strict';
    var request = require('sync-request');

    var moment = require('moment');
    var _ = require('lodash');
    var machine;
    try{
        machine = require('../../../machine.config.json');
    }catch(e){
         machine = undefined;
    }

    // function getApis(){
    //     var apiArray = _.map(Object.keys(machine.servers), function(prop){
    //         return { "name": prop, "url": machine.apis[prop]};
    //     });
    //     return apiArray;
    // }
    function getSchema(api) {
        console.log("getting schema for: " + api);
        var res = request('GET', api + 'swagger/docs/1.0');
        var schema = JSON.parse(res.getBody(), 'UTF-8');
        return schema;
    }

    function getOAuthToken(url) {
        //Username / pw stored in machine config...
        var data = "grant_type=password&username=" + machine.username + "&password=" + machine.password + "&practiceID=1&tzOffset=" + moment().format() + "&tzAbbr=CST";
        var tokenUrl = url + "/token";
        var req = request("POST",tokenUrl, {
            body: data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });
        var result;
        result = JSON.parse(req.body, 'UTF-8');

        if (!result || !result.access_token)
            throw Error('Unable to get an oauth token');
        return result.access_token;
    }

    function getValidationMetadata(entity, oToken) {
        var req = request('GET', machine.apis.content + 'api/metadata?name=' + entity,{
            'headers':{
                'Authorization':  'Bearer ' + oToken
            }
        });
        return req;
    }


    module.exports.APIHelper = {
        getSchema: getSchema,
        getOAuthToken: getOAuthToken,
        getValidationMetadata: getValidationMetadata
    };
})(module.exports);