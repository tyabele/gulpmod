(function() {
    // This generates clients for restful api's using their swagger docs
    var codeGen = require('./infinitysplitter').CodeGen;
    var scaffolder = require('./scaffold').Scaffold;
    var apihelper = require('./apihelper').APIHelper;
    var codeGenHelper = require('./generator.helper.js').CodeGenerationHelper;

    var fs = require('fs');
    var _ = require('lodash');
    function generate(_config){
        var _oToken = apihelper.getOAuthToken(_config.tokenUrl);
        _.forEach(_config.servers, function(server, _oToken){
            _generateEach(server);
        });
    }
    function _generateEach(server, _oToken) {
        var swaggerSchema = apihelper.getSchema(server.url);
        var definitions = swaggerSchema.definitions;
        var entities = codeGenHelper.getEntities(swaggerSchema);


        _.forEach(entities, function(key) {
            var swaggerObj = codeGenHelper.createSwaggerObject(swaggerSchema, key);
            //Tack on a base uri variable name...
            swaggerObj.baseURI = "common.config.uris." + server.name;
            if (!swaggerObj || _.isEmpty(swaggerObj) || swaggerObj.apis.length < 1 || _.isEmpty(swaggerObj.models))
                return;
            var code = codeGen.getAngularCode({
                className: key == 'EnumResponse' ? 'Enum' : key,
                moduleName: 'shared',
                swagger: swaggerObj
            });

            if (!_.isEmpty(code) && !_.isUndefined(code))
                codeGenHelper.writeCode(code, key);
            else
                throw new Error('Unable to generate code for ' + key);

            _.forOwn(definitions, function(value, key) {
                codeGenHelper.generateValidation(value, key);
            });
        });
    }

    function scaffold(moduleName) {
        var code = scaffold.getCode({moduleName: moduleName});
        fs.mkdirSync('./app/' + moduleName);
        writeScaffold(code);

        function writeScaffold(theCode){
            _.forEach(code, function(c) {
                if (c.Type === 'Controller') {
                    fs.writeFileSync('.public/app/' + moduleName + '/' + moduleName + '.controller.js', c.Source);
                } else if (c.Type === 'Constant') {
                    fs.writeFileSync('.public/app/' + moduleName + '/' + moduleName + '.constants.js', c.Source);
                } else if (c.Type === 'State') {
                    fs.writeFileSync('.public/app/' + moduleName + '/config.states.js', c.Source);
                } else if (c.Type === 'Module') {
                    fs.writeFileSync('.pubic/app/' + moduleName + '/' + moduleName + '.module.js', c.Source);
                }
            });
        }
    }

    module.exports =  {
        generate: generate,
        scaffold: scaffold
    };
})(module.exports);