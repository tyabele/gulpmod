/**
 * There is a lot of danger out there.
 * Generates angular factory models and data services
 * from a swagger spec file using mustache templates.
 */

/*jshint node: true*/
'use strict';

var fs = require('fs'),
    Mustache = require('mustache'),
    _ = require('lodash'),
    sourceUtility = require('./sourceutility').SourceUtility;

var camelCase = function(id) {
    var tokens = [];
    id.split('-').forEach(function(token, index) {
        if (index === 0) {
            tokens.push(token[0].toLowerCase() + token.substring(1));
        } else {
            tokens.push(token[0].toUpperCase() + token.substring(1));
        }
    });
    return tokens.join('');
};

var titleCase = function(text) {
    if(text === 'id') return 'ID';
    else {
        var firstLetter = text.substring(0, 1);
        text = text.substring(1);
        var retVal = firstLetter.toUpperCase() + text;
        if(retVal.substring(retVal.length - 2) === 'Id') {
            return retVal.substring(0, retVal.length - 2) + 'ID';
        }
        return retVal;
    }
};

var checkEnum = function(cName) {
    return cName.indexOf('EnumResponse') !== -1 || cName.indexOf('Result') !== -1 || cName.indexOf('Response') !== -1;
};

var getPathToMethodName = function(path) {
    var segments = path.split('/').slice2(1).join('-');
    return camelCase(segments);
};

var getType = function(op, name) {
    if (op.items) {
        return op.items.$ref || 'string';
    } else if (op.type !== 'IHttpActionResult') {
        return op.type;
    } else {
        return name;
    }
};

var getModel = function(swagger, modelName) {
    //have to make sure we find the right model in the oh, 100 or so models that one API root endpoint will give us)
    var modelKey = '';
    var data = {};
    for (var key in Object.keys(swagger.models)) {
        var model = swagger.models[Object.keys(swagger.models)[key]];
        if (model.id === modelName) {
            modelKey = key;
        }
    }
    if(modelKey === '' && (modelName === 'QQuestion' || modelName === 'Suites')) return {className: 'string'};
    if (modelKey === '') return {};
    // if (modelKey === '') {
    //     throw Error('Unable to find model in model hell ' + modelName);
    // }
    data.model = swagger.models[Object.keys(swagger.models)[modelKey]];
    data.properties = [];
    data.commaProperties = [];
    var properties = data.model.properties;
    var propNames = Object.keys(data.model.properties);
    for (var prop in Object.keys(properties)) {
        var p = properties[Object.keys(properties)[prop]];
        var np = { 'Name': propNames[prop], 'type': p.type, isArray: p.type === 'array' };
        data.properties.push(np);
    }
    for(var j = 0; j < propNames.length; j++) {
        var comma = propNames.length - 1 == j ? propNames[j] : propNames[j] + ',' ;
        data.commaProperties.push(comma);
    }
    data.className = modelName;
    return data;
};

function _cleanBodyParameter(parameter) {
    var complexParm = parameter.type.indexOf('[');
    if (complexParm > 0)
    parameter.type = parameter.type.substring(0, complexParm);
    return camelCase(parameter.type);
}

var getAPIData = function(opts, type) {
    var swagger = opts.swagger;
    var data = _createDataObject(swagger, opts);

    swagger.apis.forEach(function(api) {
            api.operations.forEach(function(op) {
                var mName = camelCase(op.nickname.replace(data.className + '_', '')); //Gets rid of className prefix
                data.methodNames.push(_getMethodName(mName));
                var method = _createMethod(api, opts, op, mName);

                var mod = getModel(swagger, method.item);
                if(!mod.className) mod.className = method.className;
                //Uber complex return types
                var index = mod.className.indexOf('[');
                var retType = mod.className;
                if (index > 0) {
                    retType = mod.className.substring(0, index);
                    mod.className = retType;
                    method.item = retType;
                }

                mod.moduleName = data.moduleName;
                if (method.item !== 'string' && mod.className !== 'string' && !_.find(data.modelNames, function(m) { return m === method.item; })) {
                        data.models.push(mod);
                        data.modelNames.push(method.item);
                    }
                op.parameters = op.parameters ? op.parameters : [];
                if(!method.isGET && !_.find(op.parameters, function(parm) { return parm.paramType === 'body';}))
                    op.parameters.push({
                        name: 'obj',
                        paramType: 'body',
                        type: 'obj'
                    });
                var body = _.find(op.parameters, function(parm) { return parm.paramType === 'body';});
                op.parameters.forEach(function(parameter) {
                    parameter.camelCaseName = camelCase(parameter.name);
                     parameter.titleCaseName = titleCase(parameter.name);
                    if (parameter.enum && parameter.enum.length === 1) {
                        parameter.isSingleton = true;
                        parameter.singleton = parameter.enum[0];
                    }
                    if (parameter.paramType === 'body') {
                        parameter.isBodyParameter = true;
                        parameter.camelCaseType = _cleanBodyParameter(parameter);
                    }
                    else if (parameter.paramType === 'path') {
                        parameter.isPathParameter = true;
                    }
                    else if (parameter.paramType === 'query' || parameter.paramType === 'header') {
                        if(body)
                            parameter.modelName = _cleanBodyParameter(body);
                        parameter.isQueryParameter = true;
                    }

                });

                method.hasResponse = op.type !== 'IHttpActionResult' && mod.className !== 'string' && method.item !== 'string' &&
                     mName.indexOf('ProfilePicture') < 0;
                data.methods.push(method);
            });
    });
    return data;
};

function _createDataObject(swagger, opts) {
    return {
        description: swagger.description,
        url: swagger.url,
        baseURI: swagger.baseURI,
        moduleName: opts.moduleName,
        className: opts.className,
        camelCaseClassName: camelCase(opts.className),
        methods: [],
        model: {},
        properties: [],
        commaProperties: [],
        methodNames: [], //mustache is logicless, can't dig into methods array
        models: [],
        modelNames: []
    };
}

function _createMethod(api, opts, op, mName) {
    return {
        path: api.path,
        className: opts.className,
        methodName: _getMethodName(mName),
        method: op.method,
        isGET: op.method !== 'POST' && op.method !== 'PUT',
        isPUT: op.method === 'PUT',
        summary: op.summary,
        parameters: op.parameters,
        hasResponse: false,
        responseType: opts.className,
        item: getType(op, opts.className)
    };
}

function _getMethodName(name) {
    return name === 'delete' ? 'remove' : name;
}

var getCode = function(opts, type) {
    var data = getAPIData(opts, type);
    var tpl = fs.readFileSync(__dirname + '/templates/angular-class.mustache', 'utf-8');
    var method = fs.readFileSync(__dirname + '/templates/method.mustache', 'utf-8');
    var request = fs.readFileSync(__dirname + '/templates/angular-request.mustache', 'utf-8');
    var sources = [];

    var src = Mustache.render(tpl, data, {
        method: method,
        request: request
    });
    sources.push({'Type': 'Service', 'Source': src});

    if (data.models) {
        data.models.forEach(function (md) {
            md.paged = md.className === 'PagedSearchResponse';
            md.isEnum = md.className != opts.className;
            var factory = fs.readFileSync(__dirname + '/templates/angular-factory.mustache', 'utf-8');
            var fact = Mustache.render(factory, md);
            sources.push({'Type': 'Model', 'EntityName': md.className, 'Source': fact});
        });
    }

    _.forEach(sources, function (s) {
        s.Source = sourceUtility.makePristine(s.Source);
    });
    return sources;
};

exports.CodeGen = {
    getAngularCode: function(opts) {
        return getCode(opts, 'angular');
    }
};