# README #

One gulp file to rule them all. Reusable scoped gulp tasks

# Usage #
##Install the node package##
npm install git+ssh://git@bitbucket.org:cohesity/jx-gulp-module.git --save-dev

##In your gulp file:##

var gulp = require('gulp');

var config = require('./gulp.config')();

require('jx-gulp-module')(gulp, config, __dirname);