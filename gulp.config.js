module.exports = function() {
    var base = './';
    var config = {
        //used when creating dist files
        appName: '',

        //all javascript in the app folder
        appJs: ['app/*.js', 'app/**/*.js',"!./node_modules/*"],

        //all library files from bower
        libJs: ['lib/**/*.js', '!lib/jquery/**', '!lib/bootstrap/**', '!lib/jquery-ui/**'],

        //order of lib dependencies
        libJsOrder: '',

        appCss: ['assets/styles.css', 'assets/styles/colorstyle.css'],

        libCss: ['lib/toastr/toastr.css', 'assets/bootstrap.css'],

        commonConfigFile: 'app/common/common.config.js',

        //base path
        base: base,

        injectBase: 'views/includes/head.jade',

        injectDest: './public',

        index: 'index.html',

        htmlTemplates: 'app/**/*.html',

        JuxlyServers: [],

        karmaConfig: './tests/unit/karma.conf.js',

        // Spec patterns are relative to the current working directly when
        // protractor is called.
        specs: '',

        /**
         * Template Cache Options
         */
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'app',
                standAlone: false,
                root: 'app/'
            }
        },

        timelineAppName: 'juxly-timeline',
        webRoot: ''
    };
    return config;
};
